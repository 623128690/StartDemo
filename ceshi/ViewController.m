//
//  ViewController.m
//  ceshi
//
//  Created by diannao on 2017/12/6.
//  Copyright © 2017年 heliweimin. All rights reserved.
//
//
//#import <Masonry.h>
#import "Masonry.h"
#import "ViewController.h"
#import <UIKit/UIKit.h>
#import "WJTabBarController.h"
@interface ViewController ()


@end


@implementation ViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.view.backgroundColor = [UIColor redColor];
    [self creatUI];
}
- (void)viewWillAppear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:YES animated:NO];
}
- (void)creatUI{
    
    UIButton *but = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [but setTitle:@"登录" forState:(UIControlStateNormal)];
    [but setBackgroundColor:WJColor];
    [but addTarget:self action:@selector(butAction) forControlEvents:(UIControlEventTouchUpInside)];
    [self.view addSubview:but];
    [but mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(100, 50));
        if (@available(iOS 11.0, *)) {
            make.center.mas_equalTo(self.view.safeAreaInsets);
        } else {
            make.center.mas_equalTo(self.view);
        }
    }];
    
    
}
- (void)butAction{
    WJTabBarController * vv = [[WJTabBarController alloc] init];
    [self.navigationController pushViewController:vv animated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
