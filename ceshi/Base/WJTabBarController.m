//
//  WJTabBarController.m
//  WhoJob
//
//  Created by diannao on 2017/6/20.
//  Copyright © 2017年 heliweimin. All rights reserved.
//

#import "WJTabBarController.h"
#import "WJNavgationController.h"
#import "QkoneViewController.h"
#import "QktwoViewController.h"
#import "QkthreeViewController.h"

@interface WJTabBarController ()

@property (nonatomic,assign) NSInteger  indexFlag;

@end

@implementation WJTabBarController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.indexFlag = 0;
    [[UITabBar appearance] setBarTintColor:[UIColor whiteColor]];
    [UITabBar appearance].translucent = NO;
    // 2.创建自控制器
    [self setUpAllChildViewController];
  
    
}
- (void)setUpAllChildViewController{
    QkoneViewController *one = [[QkoneViewController alloc] init];
    QktwoViewController *two = [[QktwoViewController alloc] init];
    QkthreeViewController *three = [[QkthreeViewController alloc] init];
    NSArray *arr1 = @[one,two,three];
    NSArray *arr2 = @[@"tabbar_职位_1",@"tabbar_公司_1",@"tabbar_我的_1"];
    NSArray *arr3 = @[@"tabbar_职位_2",@"tabbar_公司_2",@"tabbar_我的_2"];
    NSArray *arr4 = @[@"职位",@"公司",@"我的"];
    for (int i = 0; i < arr1.count; i++) {
        [self setUpOneChildViewController:arr1[i] image:[UIImage imageNamed:arr2[i]] selectImage:[UIImage imageNamed:arr3[i]] title:arr4[i]];
    }
  
}

- (void)setUpOneChildViewController:(UIViewController *)viewController image:(UIImage *)image selectImage:(UIImage *)selectImage title:(NSString *)title {
    //每个bar上面添加导航栏
    WJNavgationController *navC = [[WJNavgationController alloc]initWithRootViewController:viewController];
    navC.tabBarItem.title = title;
    NSMutableDictionary *textAttrs = [NSMutableDictionary dictionary];
    textAttrs[NSForegroundColorAttributeName] = RGBA(0, 0, 0, 0.4);
    NSMutableDictionary *selectTextAttrs = [NSMutableDictionary dictionary];
    selectTextAttrs[NSForegroundColorAttributeName] = WJColor;
    [navC.tabBarItem setTitleTextAttributes:textAttrs forState:UIControlStateNormal];
    [navC.tabBarItem setTitleTextAttributes:selectTextAttrs forState:UIControlStateSelected];
    viewController.title = title;
    navC.tabBarItem.image = image;
    navC.tabBarItem.selectedImage = [selectImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
//    navC.tabBarItem.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0);
    [self addChildViewController:navC];
}

-(void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item{

    NSInteger index = [self.tabBar.items indexOfObject:item];
    if (index != self.indexFlag) {
        //执行动画
        NSMutableArray *arry = [NSMutableArray array];
        for (UIView *btn in self.tabBar.subviews) {
            if ([btn isKindOfClass:NSClassFromString(@"UITabBarButton")]) {
                [arry addObject:btn];
            }
        }
        //添加动画
        //---将下面的代码块直接拷贝到此即可---
        //放大效果，并回到原位
        CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
        //速度控制函数，控制动画运行的节奏
        animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        animation.duration = 0.2;       //执行时间
        animation.repeatCount = 1;      //执行次数
        animation.autoreverses = YES;    //完成动画后会回到执行动画之前的状态
        animation.fromValue = [NSNumber numberWithFloat:0.7];   //初始伸缩倍数
        animation.toValue = [NSNumber numberWithFloat:1.3];     //结束伸缩倍数
        [[arry[index] layer] addAnimation:animation forKey:nil];
        self.indexFlag = index;
    }
    
    
}





- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
