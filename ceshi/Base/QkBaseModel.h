//
//  QkBaseModel.h
//  ceshi
//
//  Created by diannao on 2017/12/11.
//  Copyright © 2017年 heliweimin. All rights reserved.
//
//http://blog.csdn.net/qq_30402119/article/details/50605918

#import <Foundation/Foundation.h>

@interface QkBaseModel : NSObject

/* 这里也可以添加每个地方都要用到的属性 比如id,name等等, 直接以正常属性书写方式写在这里即可 */
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *nID; /* 这里的参数以自己工程为准, 每个接口都有的参数可以写在这里 */
+ (id)modelWithDictionary:(NSDictionary *)dic; /** 这是公用的过滤接口数据的方法 */

/*
1.启动图 （LaunchImage ），另外需要一张 1125 * 2436 适配iPhone X
2.tarbar 底部按钮图
3.求职者和企业 （我的）八张图里面新改的几张
4.视频 里面的图片
5.产品 上线的图片
6.还有一些换色之后的小按钮（下拉等）
*/
@end
