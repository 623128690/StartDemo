//
//  WJNavgationController.m
//  WhoJob
//
//  Created by diannao on 2017/6/20.
//  Copyright © 2017年 heliweimin. All rights reserved.
//;
#import "WJNavgationController.h"

@interface WJNavgationController ()<UIGestureRecognizerDelegate,UINavigationControllerDelegate>

@end

@implementation WJNavgationController

- (void)viewDidLoad {
    [super viewDidLoad];
   
    [self.navigationBar setBarTintColor:WJColor];
    [[UINavigationBar appearance] setTranslucent:NO];
    [self.navigationBar setTintColor:[UIColor whiteColor]];
    [self.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName:[UIFont systemFontOfSize:16]}];
    //    UIPanGestureRecognizer *pan = [[UIPanGestureRecognizer alloc] initWithTarget:self.interactivePopGestureRecognizer.delegate action:@selector(handleNavigationTransition:)];
    //    [self.view addGestureRecognizer:pan];
    //    // 控制手势什么时候触发,只有非根控制器才需要触发手势
    //    pan.delegate = self;
    [self.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    self.navigationBar.barStyle = UIBarStyleDefault;
    self.navigationBar.translucent = NO;
    [self.navigationBar setShadowImage:[UIImage new]];
    self.extendedLayoutIncludesOpaqueBars = YES;
    // 禁止之前手势
//    self.interactivePopGestureRecognizer.enabled = NO;
    // 假死状态:程序还在运行,但是界面死了.
    self.interactivePopGestureRecognizer.delegate = self;
    
    // Do any additional setup after loading the view.
}

//手势对应的事件
- (void)handleNavigationTransition:(UIPanGestureRecognizer *)pan{
    [self popViewControllerAnimated:YES];
}

#pragma mark - UIGestureRecognizerDelegate
// 决定是否触发手势
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    return self.childViewControllers.count > 1;
}

-(void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated{
    if (self.childViewControllers.count > 0) {
        // 非根控制器
        viewController.hidesBottomBarWhenPushed = YES;
        // 设置返回按钮,只有非根控制器
         viewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"返回键"] style:UIBarButtonItemStyleDone target:self action:@selector(back)];
    }
    // 真正在跳转
    [super pushViewController:viewController animated:animated];
}

- (void)back
{
    [self popViewControllerAnimated:YES];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
