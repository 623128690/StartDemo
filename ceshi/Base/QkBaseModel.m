//
//  QkBaseModel.m
//  ceshi
//
//  Created by diannao on 2017/12/11.
//  Copyright © 2017年 heliweimin. All rights reserved.
//

#import "QkBaseModel.h"

@implementation QkBaseModel

- (void)setValue:(id)value forUndefinedKey:(NSString *)key{
    if ([key isEqualToString:@"id"]) {
        self.nID = value;
    } /* 如果参数的key 是系统关键字的话需要在这里过滤一下, 把value赋给我们自己声明的属性即可 */
}
/* 根据数据字典返回model */
+ (id)modelWithDictionary:(NSDictionary *)dic {
    __strong Class model = [[[self class] alloc] init];
    [model setValuesForKeysWithDictionary:dic];
    return model;
}

@end
